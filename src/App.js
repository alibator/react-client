import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import { 
	Card, CardImg, CardText, ListGroup, ListGroupItem,
 	CardBody, CardTitle, Button, 
	Container, Row, Col,
	Navbar, Collapse, NavbarToggler,
	NavbarBrand, Nav, NavItem, NavLink, Badge,
	 } from 'reactstrap';
import {
  BrowserRouter as Router,
  Route,
  Link, Switch
} from 'react-router-dom'
import ReactPlaceholder from 'react-placeholder';
import {TextBlock, MediaBlock, TextRow, RectShape, RoundShape} from 'react-placeholder/lib/placeholders';

class AppNav extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}


	render() {
		return (
			<Navbar color="faded" light expand="md">
				<Link to="/">
					<NavbarBrand>alibator</NavbarBrand>
				</Link>
				<NavbarToggler onClick={this.toggle} />
				<Collapse isOpen={this.state.isOpen} navbar>
					<Nav className="ml-auto" navbar>
						<NavItem>
							<NavLink href="/contacts/">
								Контакты
							</NavLink>
						</NavItem>
					</Nav>
				</Collapse>
			</Navbar>
		)
	}
}

const categoryPlaceholder = (
	<div className="placeholder">
		<RectShape color='#CDCDCD' style={{width: '85%', height: 30}}/>
		<br/>
		<RectShape className="no-padding" color='#CDCDCD' style={{width: '100%', height: 150}}/>
		<br/>
		<TextBlock color='#CDCDCD' rows={7}/>
	</div>
);

class Category extends Component {
	items = [];

	constructor(props) {
		super(props);
		this.state = {ready: false};
	}

	render() {
		return (
			<Card>
				<ReactPlaceholder customPlaceholder={categoryPlaceholder} ready={this.state.ready}>
					<CardBody>
						<CardTitle>{this.state.name}</CardTitle>
					</CardBody>
					<img width={"100%"} src={this.state.img}/>
					<CardBody>
						<CardText>{this.state.description}</CardText>
						{this.props.preview ? (
							<Link to={`/categories/${this.props.id}`}>
								<Button color="primary">{this.items.length} товара</Button>
							</Link>
						) : (
							<ListGroup>
								{this.items.map((number) =>
									<ListGroupItem>{number.name}</ListGroupItem>
								)}
							</ListGroup>
						)}
					</CardBody>
				</ReactPlaceholder>
			</Card>
		);
	}

	componentDidMount() {
		let self = this;
		axios.get("http://localhost:8000/api/categories/"+self.props.id+"/?format=json").then(function (response) {
			self.items = response.data.item_set;
			self.setState(response.data);
			self.setState({ready: true})
		}).catch(function (error) {
			console.log(error);
		});
	}
}

const Categories = ({ match }) => (
	<div>
		<Container>
			<Row>
				<Col xs="6" sm="4">
					<Category id={1} preview={true}/>
				</Col>
				<Col xs="6" sm="4">
					<Category id={2} preview={true}/>
				</Col>
				<Col xs="6" sm="4">
					<Category id={3} preview={true}/>
				</Col>
			</Row>
		</Container>
	</div>
)

const ShowCategory = ({ match }) => (
	<div>
		<Container>
			<Row>
				<Col xs="8" sm={{ size: 6, offset: 3 }}>
					<Category id={match.params.id} preview={false}/>
				</Col>
			</Row>
		</Container>
	</div>
)

class App extends Component {
	render() {
		return (
			<Router>
				<div>
					<Container>
						<AppNav/>
					</Container>
					<Switch>
						<Route exact path="/" component={Categories}/>
						<Route path={`/categories/:id`} component={ShowCategory}/>
					</Switch>
				</div>
			</Router>
		);
	}
}


export default App;
